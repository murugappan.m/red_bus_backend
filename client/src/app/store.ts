import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import deleteTicketSlice from '../features/bookings/bookingSlice'
import bookingsSlice from '../features/bookings/bookingsSlice'
import authReducer from '../features/login/authSlice'
import loginSlice from '../features/login/loginSlice'
import registerSlice from '../features/register/registerSlice'
import searchFormSlice from '../features/search/searchFormSlice'
import searchSlice from '../features/search/searchSlice'
import seatsSlice from '../features/seat/seatsSlice'
import ticketSlice from '../features/ticket/ticketSlice'

export const store = configureStore({
    reducer: {
        registerForm: registerSlice,
        loginForm: loginSlice,
        searchForm: searchFormSlice,
        auth: authReducer,
        trips: searchSlice,
        seats: seatsSlice,
        bookings: bookingsSlice,
        ticket: ticketSlice,
        deleteTicket: deleteTicketSlice,
    },
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>
