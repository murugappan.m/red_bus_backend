import { CircularProgress } from '@mui/material'
import { Navigate, Outlet, useLocation } from 'react-router-dom'
import { useAppSelector } from '../app/hooks'
import { selectAuth } from '../features/login/authSlice'
import Alerts from './alert/Alerts'
import NavBar from './Navbar'

export default function ProtectedRoute() {
    const location = useLocation()
    const { user, isLoading, errors } = useAppSelector(selectAuth)
    return isLoading ? (
        <CircularProgress />
    ) : errors ? (
        <Alerts errors={errors} />
    ) : user ? (
        <>
            <NavBar />
            <Outlet />
        </>
    ) : (
        <Navigate to="/signin" replace state={location.pathname} />
    )
}
