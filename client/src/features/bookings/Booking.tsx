import {
    Card,
    CardContent,
    Typography,
    CardActions,
    Button,
    CircularProgress,
} from '@mui/material'
import React, { useState } from 'react'
import { Ticket } from './bookingInterface'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import ExpandLessIcon from '@mui/icons-material/ExpandLess'
import './booking.css'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { deleteTicket, selectDeleteTicket } from './bookingSlice'
import Alerts from '../../components/alert/Alerts'

export default function Booking({ ticket }: { ticket: Ticket }) {
    const dispatch = useAppDispatch()
    const { ticketId, isLoading, errors } = useAppSelector(selectDeleteTicket)
    const [isShow, setIsShow] = useState(false)
    const toggleShow = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) =>
        setIsShow(!isShow)
    const handleCancel = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) =>
        dispatch(deleteTicket(ticket._id))
    return (
        <Card className="ticket-card">
            <CardContent>
                <Typography variant="h5" component="div">
                    {ticket.from} - {ticket.to}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    departure - {ticket.startDateTime}
                    <br />
                    arrival - {ticket.endDateTime}
                </Typography>
                <Typography variant="body2">
                    busNo - {ticket.bus.busNo}
                    <br />
                    type - {ticket.bus.type}
                    <br />
                    {ticket.bus.ac ? 'A/C' : 'non A/C'}
                </Typography>
                {isShow &&
                    ticket.seats.map((seat, idx) => {
                        return (
                            <div key={idx}>
                                <h5>seatNo: {seat.seatNo}</h5>
                                <b>price:</b> {seat.price}
                                <br />
                                <b>passenger name:</b>
                                {seat.passenger.name}
                            </div>
                        )
                    })}
            </CardContent>
            <CardActions>
                <Button size="small" onClick={toggleShow}>
                    {isShow ? (
                        <ExpandLessIcon />
                    ) : (
                        <>
                            <span>Seat Details</span>
                            <ExpandMoreIcon />
                        </>
                    )}
                </Button>
                {ticketId === ticket._id ? (
                    errors ? (
                        <Alerts errors={errors} />
                    ) : (
                        <></>
                    )
                ) : isLoading ? (
                    <CircularProgress />
                ) : (
                    <Button
                        variant="contained"
                        color="error"
                        onClick={handleCancel}
                    >
                        Cancel ticket
                    </Button>
                )}
            </CardActions>
        </Card>
    )
}
