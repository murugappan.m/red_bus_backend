import CircularProgress from '@mui/material/CircularProgress'
import React, { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import Alerts from '../../components/alert/Alerts'
import Booking from './Booking'
import { getBookings, selectBookings } from './bookingsSlice'

export default function Bookings() {
    const { bookings, isLoading, errors } = useAppSelector(selectBookings)
    const dispatch = useAppDispatch()
    useEffect(() => {
        dispatch(getBookings())
    }, [dispatch])
    if (isLoading) return <CircularProgress />
    if (errors) return <Alerts errors={errors} />
    return (
        <>
            <h1>Your Bookings</h1>
            <div className="felx flex-column flex-wrap md:flex-row">
                {bookings?.map((ticket) => (
                    <Booking ticket={ticket} />
                ))}
            </div>
        </>
    )
}
