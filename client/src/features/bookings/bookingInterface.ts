import type { Error } from '../../components/alert/alertInterface'
import { Bus } from '../search/tripInterface'
import { ISeat } from '../seat/seatsInterface'

export interface Ticket {
    _id: string
    from: string
    to: string
    bus: Bus
    startDateTime: string
    endDateTime: string
    seats: IBookedSeat[]
}

export interface IBookedSeat extends ISeat {
    passenger: Passenger
}

export interface Passenger {
    name: string
    gender: string
    age: number
}

export interface BookingState {
    bookings: Ticket[] | null
    errors: Error[] | null
    isLoading: boolean
}

export interface DeleteTicket {
    ticketId: string | null
    isLoading: boolean
    errors: Error[] | null
}
