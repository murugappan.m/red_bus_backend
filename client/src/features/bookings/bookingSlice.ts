import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { AxiosError } from 'axios'
import { RootState } from '../../app/store'
import { Error } from '../../components/alert/alertInterface'
import { DeleteTicket } from './bookingInterface'
import { deleteTicketApi } from './bookingsAPI'
import { getBookings } from './bookingsSlice'

const initialState: DeleteTicket = {
    ticketId: null,
    isLoading: false,
    errors: null,
}

export const deleteTicket = createAsyncThunk(
    'ticket/delete',
    async (_id: string, { dispatch, rejectWithValue }) => {
        try {
            await deleteTicketApi(_id)
            dispatch(getBookings())
            return _id
        } catch (err: unknown) {
            if (!(err instanceof AxiosError) || !err.response) {
                throw err
            }
            return rejectWithValue(err.response.data.errors)
        }
    }
)

export const deleteTicketSlice = createSlice({
    name: 'bookings',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(deleteTicket.pending, () => {
                return { ...initialState, isLoading: true }
            })
            .addCase(deleteTicket.fulfilled, (state, action) => {
                state.isLoading = false
                state.ticketId = action.payload
            })
            .addCase(deleteTicket.rejected, (state, action) => {
                state.isLoading = false
                state.errors = action.payload as Error[]
            })
    },
})

export const selectDeleteTicket = (state: RootState) => state.deleteTicket

export default deleteTicketSlice.reducer
