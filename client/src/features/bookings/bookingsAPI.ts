import api from '../../utils/api'

export async function getBookingsApi() {
    const { data } = await api.get('/ticket')
    return data
}

export async function deleteTicketApi(_id: string) {
    await api.delete(`/ticket/${_id}`)
}
