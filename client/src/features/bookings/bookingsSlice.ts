import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { AxiosError } from 'axios'
import { RootState } from '../../app/store'
import type { Error } from '../../components/alert/alertInterface'
import { BookingState } from './bookingInterface'
import { getBookingsApi } from './bookingsAPI'

const initialState: BookingState = {
    bookings: null,
    isLoading: false,
    errors: null,
}

export const getBookings = createAsyncThunk(
    'ticket/getAll',
    async (_, { rejectWithValue }) => {
        try {
            const data = await getBookingsApi()
            return data
        } catch (err: unknown) {
            if (!(err instanceof AxiosError) || !err.response) {
                throw err
            }
            return rejectWithValue(err.response.data.errors)
        }
    }
)

export const bookingsSlice = createSlice({
    name: 'bookings',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getBookings.pending, () => {
                return { ...initialState, isLoading: true }
            })
            .addCase(getBookings.fulfilled, (state, action) => {
                state.isLoading = false
                state.bookings = action.payload
            })
            .addCase(getBookings.rejected, (state, action) => {
                state.isLoading = false
                state.errors = action.payload as Error[]
            })
    },
})

export const selectBookings = (state: RootState) => state.bookings

export default bookingsSlice.reducer
