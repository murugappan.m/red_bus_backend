import {
    Box,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormGroup,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    TextField,
} from '@mui/material'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker'
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment'
import Button from '@mui/material/Button'
import { Moment } from 'moment'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { searchTrips } from './searchSlice'
import TripsTable from './Trips'
import React from 'react'
import {
    selectSearchForm,
    setSearchAc,
    setSearchDate,
    setSearchFrom,
    setSearchTo,
    setSearchType,
} from './searchFormSlice'

export default function Search() {
    const dispatch = useAppDispatch()
    const { from, to, date, type, ac } = useAppSelector(selectSearchForm)
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        dispatch(searchTrips())
    }
    const changeFrom = (
        e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => dispatch(setSearchFrom(e.target.value))
    const changeTo = (
        e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => dispatch(setSearchTo(e.target.value))
    const changeDate = (newDate: Moment | null) =>
        dispatch(setSearchDate(newDate as Moment))
    const changeAc = (e: React.ChangeEvent<HTMLInputElement>) =>
        dispatch(setSearchAc(e.target.checked))
    const changeType = (e: SelectChangeEvent<string | File | null>) =>
        dispatch(setSearchType(e.target.value as string))
    return (
        <div>
            <Box
                component="form"
                noValidate
                onSubmit={handleSubmit}
                sx={{ mt: 3 }}
            >
                <div className="flex flex-col justify-center gap-3 m-1 p-5 sm:flex-row">
                    <TextField
                        autoComplete="from"
                        value={from}
                        onChange={changeFrom}
                        required
                        label="from"
                        autoFocus
                    />
                    <TextField
                        autoComplete="to"
                        value={to}
                        onChange={changeTo}
                        required
                        label="to"
                    />
                    <LocalizationProvider dateAdapter={AdapterMoment}>
                        <DesktopDatePicker
                            label="Journey date"
                            inputFormat="MM/DD/YYYY"
                            value={date}
                            onChange={changeDate}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                    <FormControlLabel
                        control={<Checkbox value={ac} onChange={changeAc} />}
                        label="ac"
                    />
                    <FormControl>
                        <InputLabel id="type-trip-label">Type</InputLabel>
                        <Select
                            labelId="type-trip-label"
                            id="type"
                            name="type"
                            label="Type"
                            value={type}
                            onChange={changeType}
                        >
                            <MenuItem value={'None'}>None</MenuItem>
                            <MenuItem value={'Seater'}>Seater</MenuItem>
                            <MenuItem value={'Sleeper'}>Sleeper</MenuItem>
                            <MenuItem value={'SeaterSleeper'}>
                                SeaterSleeper
                            </MenuItem>
                        </Select>
                    </FormControl>
                    <Button
                        type="submit"
                        variant="contained"
                        sx={{ mt: 1, mb: 1 }}
                    >
                        Search
                    </Button>
                </div>
            </Box>
            <TripsTable />
        </div>
    )
}
