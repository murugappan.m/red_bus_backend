import * as React from 'react'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import Alerts from '../../components/alert/Alerts'
import LinearProgress from '@mui/material/LinearProgress'
import { Button } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import { useAppSelector } from '../../app/hooks'
import { selectTrips } from './searchSlice'

export default function TripsTable() {
    const { trips, errors, isLoading } = useAppSelector(selectTrips)
    const navigate = useNavigate()
    if (isLoading) return <LinearProgress />
    if (errors) return <Alerts errors={errors} />
    if (!trips) return <div></div>
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="trips table">
                <TableHead>
                    <TableRow>
                        <TableCell>From</TableCell>
                        <TableCell align="right">To</TableCell>
                        <TableCell align="right">StartDateTime</TableCell>
                        <TableCell align="right">EndDateTime</TableCell>
                        <TableCell align="right">BusNo</TableCell>
                        <TableCell align="right">Type</TableCell>
                        <TableCell align="right">Total Seats</TableCell>
                        <TableCell align="right">Available Seats</TableCell>
                        <TableCell align="right">Price Range</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {trips.map((trip) => (
                        <TableRow
                            key={trip._id}
                            sx={{
                                '&:last-child td, &:last-child th': {
                                    border: 0,
                                },
                            }}
                        >
                            <TableCell component="th" scope="row">
                                {trip.from}
                            </TableCell>
                            <TableCell align="right">{trip.to}</TableCell>
                            <TableCell align="right">
                                {trip.startDateTime}
                            </TableCell>
                            <TableCell align="right">
                                {trip.endDateTime}
                            </TableCell>
                            <TableCell align="right">
                                {trip.bus.busNo}
                            </TableCell>
                            <TableCell align="right">{trip.bus.type}</TableCell>
                            <TableCell align="right">
                                {trip.totalSeats}
                            </TableCell>
                            <TableCell align="right">
                                {trip.availableSeat}
                            </TableCell>
                            <TableCell align="right">
                                {trip.minPrice}
                                {trip.minPrice !== trip.maxPrice &&
                                    ' - ' + trip.maxPrice}
                            </TableCell>
                            <TableCell align="right">
                                <Button
                                    onClick={() =>
                                        navigate(`/seats/${trip._id}`)
                                    }
                                >
                                    view Seats
                                </Button>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}
