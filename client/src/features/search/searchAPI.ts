import api from '../../utils/api'
import { searchQuery } from './searchInterface'

export async function searchTripsApi(queryParams: searchQuery) {
    const { data } = await api.get('/searchTrip', {
        params: queryParams,
    })
    return data
}
