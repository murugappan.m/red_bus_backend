import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import moment, { Moment } from 'moment'
import { RootState } from '../../app/store'
import { searchForm } from './searchInterface'

const initialState: searchForm = {
    from: '',
    to: '',
    date: moment(),
    ac: false,
    type: 'None',
}

export const searchFormSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setSearchFrom: (state, action: PayloadAction<string>) => {
            state.from = action.payload
        },
        setSearchTo: (state, action: PayloadAction<string>) => {
            state.to = action.payload
        },
        setSearchDate: (state, action: PayloadAction<Moment>) => {
            state.date = action.payload
        },
        setSearchAc: (state, action: PayloadAction<boolean>) => {
            state.ac = action.payload
        },
        setSearchType: (state, action: PayloadAction<string>) => {
            state.type = action.payload
        },
    },
})

export const {
    setSearchFrom,
    setSearchTo,
    setSearchDate,
    setSearchAc,
    setSearchType,
} = searchFormSlice.actions

export const selectSearchForm = (state: RootState) => state.searchForm

export default searchFormSlice.reducer
