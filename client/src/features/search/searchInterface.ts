import { Moment } from 'moment'

export interface searchForm {
    from: FormDataEntryValue
    to: FormDataEntryValue
    date: Moment
    ac: boolean
    type: FormDataEntryValue
}

export interface searchQuery {
    from: FormDataEntryValue
    to: FormDataEntryValue
    date: string
    ac: boolean | null
    type: FormDataEntryValue | null
}
