import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { AxiosError } from 'axios'
import moment from 'moment'
import { RootState } from '../../app/store'
import type { Error } from '../../components/alert/alertInterface'
import { searchTripsApi } from './searchAPI'
import { Trip, TripState } from './tripInterface'

const initialState: TripState = {
    trips: null,
    isLoading: false,
    errors: null,
}

export const searchTrips = createAsyncThunk<
    Trip[] | Error[],
    undefined,
    { state: RootState }
>('trip/searchUser', async (_, { getState, rejectWithValue }) => {
    try {
        const { searchForm } = getState()
        const curTime = moment().utc().format('HH:mm:ss')
        const queryParams = {
            ...searchForm,
            date: searchForm.date
                .utc()
                .format('YYYY-MM-DDT')
                .concat(curTime)
                .concat('Z'),
            ac: searchForm.ac ? true : null,
            type: searchForm.type !== 'None' ? searchForm.type : null,
        }
        const trips = await searchTripsApi(queryParams)
        return trips
    } catch (err: unknown) {
        if (!(err instanceof AxiosError) || !err.response) {
            throw err
        }
        return rejectWithValue(err.response.data.errors)
    }
})

export const searchSlice = createSlice({
    name: 'searchTrips',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(searchTrips.pending, (state) => {
                return { ...initialState, isLoading: true }
            })
            .addCase(searchTrips.fulfilled, (state, action) => {
                state.isLoading = false
                state.trips = action.payload as Trip[]
            })
            .addCase(searchTrips.rejected, (state, action) => {
                state.isLoading = false
                state.errors = action.payload as Error[]
            })
    },
})

export const selectTrips = (state: RootState) => state.trips

export default searchSlice.reducer
