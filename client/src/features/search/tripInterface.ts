import type { Error } from '../../components/alert/alertInterface'

export interface Trip {
    _id: string
    bus: Bus
    from: string
    to: string
    startDateTime: string
    endDateTime: string
    totalSeats: number
    availableSeat: number
    minPrice: number
    maxPrice: number
}

export interface Bus {
    busNo: string
    type: string
    ac: boolean
}

export interface TripState {
    trips: Trip[] | null
    isLoading: boolean
    errors: Error[] | null
}
