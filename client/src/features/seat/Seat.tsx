import { IconButton, Tooltip } from '@mui/material'
import React from 'react'
import AirlineSeatIndividualSuiteIcon from '@mui/icons-material/AirlineSeatIndividualSuite'
import AirlineSeatReclineNormalIcon from '@mui/icons-material/AirlineSeatReclineNormal'
import { ISeatShow } from './seatsInterface'

export default function Seat({
    seat,
    seatNos,
    onClick,
}: {
    seat: ISeatShow
    seatNos: Set<string>
    onClick: (seatNo: string) => void
}) {
    return (
        <Tooltip
            title={`seatNo: ${seat.seatNo}, Price: ${seat.price}, ${
                seat.type.single ? 'single' : 'double'
            } ${seat.type.sleeper ? 'sleeper' : 'seater'} `}
        >
            <IconButton
                disabled={seat.isBooked}
                onClick={(e) => onClick(seat.seatNo)}
                color={seatNos.has(seat.seatNo) ? 'success' : 'inherit'}
            >
                {seat.type.sleeper ? (
                    <AirlineSeatIndividualSuiteIcon />
                ) : (
                    <AirlineSeatReclineNormalIcon />
                )}
            </IconButton>
        </Tooltip>
    )
}
