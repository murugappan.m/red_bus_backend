import React, { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { getSeats, selectSeats } from './seatsSlice'
import { Link, useParams } from 'react-router-dom'
import { Box, Button, CircularProgress, LinearProgress } from '@mui/material'
import Passenger from '../ticket/Passenger'
import Alerts from '../../components/alert/Alerts'
import Seat from './Seat'
import {
    addSeatNo,
    postTicket,
    removeSeatNo,
    resetTicket,
    selectTicket,
} from '../ticket/ticketSlice'
import './seats.css'

export default function Seats() {
    const { tripId } = useParams()
    const { seats, errors, isLoading } = useAppSelector(selectSeats)
    const {
        bookSeatDetails,
        ticketId,
        errors: ticketErrors,
        isLoading: isTicketLoading,
    } = useAppSelector(selectTicket)
    const dispatch = useAppDispatch()
    useEffect(() => {
        if (tripId) dispatch(getSeats(tripId))
    }, [tripId, dispatch])
    const upperSeats = seats?.filter((seat) => seat.type.upper)
    const lowerSeats = seats?.filter((seat) => !seat.type.upper)
    const seatNos = new Set<string>(
        bookSeatDetails?.map((ticket) => ticket.seatNo)
    )
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        dispatch(postTicket(tripId))
    }
    const updateSelect = (seatNo: string) => {
        if (seatNos.has(seatNo)) dispatch(removeSeatNo(seatNo))
        else dispatch(addSeatNo(seatNo))
    }
    if (isLoading) return <LinearProgress />
    if (errors) return <Alerts errors={errors} />
    return (
        <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            className="flex-column"
        >
            {upperSeats?.length !== 0 && (
                <div className="custom-container">
                    Upper Deck
                    <div>
                        {upperSeats?.map((seat, idx) => {
                            if (seat.type.single)
                                return (
                                    <Seat
                                        seat={seat}
                                        seatNos={seatNos}
                                        key={idx}
                                        onClick={(seatNo: string) =>
                                            updateSelect(seatNo)
                                        }
                                    />
                                )
                            return <></>
                        })}
                    </div>
                    <div>
                        {upperSeats?.map((seat, idx) => {
                            if (!seat.type.single)
                                return (
                                    <Seat
                                        seat={seat}
                                        seatNos={seatNos}
                                        key={idx}
                                        onClick={(seatNo: string) =>
                                            updateSelect(seatNo)
                                        }
                                    />
                                )
                            return <></>
                        })}
                    </div>
                </div>
            )}
            {lowerSeats?.length !== 0 && (
                <div className="custom-container">
                    Lower Deck
                    <div>
                        {lowerSeats?.map((seat, idx) => {
                            if (seat.type.sleeper)
                                return (
                                    <Seat
                                        seat={seat}
                                        seatNos={seatNos}
                                        key={idx}
                                        onClick={(seatNo: string) =>
                                            updateSelect(seatNo)
                                        }
                                    />
                                )
                            return <></>
                        })}
                    </div>
                    <div>
                        {lowerSeats?.map((seat, idx) => {
                            if (!seat.type.sleeper)
                                return (
                                    <Seat
                                        seat={seat}
                                        seatNos={seatNos}
                                        key={idx}
                                        onClick={(seatNo: string) =>
                                            updateSelect(seatNo)
                                        }
                                    />
                                )
                            return <></>
                        })}
                    </div>
                </div>
            )}
            <Passenger />
            {isTicketLoading ? (
                <CircularProgress />
            ) : ticketId ? (
                <>
                    <h1>
                        Booking success <br />
                        ticket ID: {ticketId}
                    </h1>
                    <Link to="/bookings">View All Bookings</Link>
                    <Button onClick={() => dispatch(resetTicket())}>
                        Book new Ticket
                    </Button>
                </>
            ) : (
                <Button variant="contained" type="submit">
                    Book Ticket
                </Button>
            )}
            {ticketErrors && <Alerts errors={ticketErrors} />}
        </Box>
    )
}
