import api from '../../utils/api'

export async function getSeatsApi(tripId: string) {
    const { data } = await api.get(`/seats/${tripId}`)
    return data
}
