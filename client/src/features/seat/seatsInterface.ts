import type { Error } from '../../components/alert/alertInterface'

export interface SeatsState {
    seats: ISeatShow[] | null
    isLoading: boolean
    errors: Error[] | null
}

export interface ISeatShow extends ISeat {
    isBooked: boolean
}

export interface ISeat {
    seatNo: string
    type: {
        sleeper: boolean
        upper: boolean
        single: boolean
    }
    price: number
}
