import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { AxiosError } from 'axios'
import { RootState } from '../../app/store'
import type { Error } from '../../components/alert/alertInterface'
import { getSeatsApi } from './seatAPI'
import { SeatsState } from './seatsInterface'

const initialState: SeatsState = {
    seats: null,
    isLoading: false,
    errors: null,
}

export const getSeats = createAsyncThunk(
    'trip/searchSeat',
    async (tripId: string, { rejectWithValue }) => {
        try {
            const data = await getSeatsApi(tripId)
            return data
        } catch (err: unknown) {
            if (!(err instanceof AxiosError) || !err.response) {
                throw err
            }
            return rejectWithValue(err.response.data.errors)
        }
    }
)

export const seatsSlice = createSlice({
    name: 'seats',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getSeats.pending, (state) => {
                return { ...initialState, isLoading: true }
            })
            .addCase(getSeats.fulfilled, (state, action) => {
                state.isLoading = false
                state.seats = action.payload
            })
            .addCase(getSeats.rejected, (state, action) => {
                state.isLoading = false
                state.errors = action.payload as Error[]
            })
    },
})

export const selectSeats = (state: RootState) => state.seats

export default seatsSlice.reducer
