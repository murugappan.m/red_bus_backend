import {
    Box,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    TextField,
} from '@mui/material'
import React from 'react'
import { useDispatch } from 'react-redux'
import { useAppSelector } from '../../app/hooks'
import {
    selectTicket,
    setPassengerAge,
    setPassengerGender,
    setPassengerName,
} from './ticketSlice'

export default function Passenger() {
    const dispatch = useDispatch()
    const { bookSeatDetails } = useAppSelector(selectTicket)
    const changeName = (
        e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
        seatNo: string
    ) => dispatch(setPassengerName({ seatNo, name: e.target.value as string }))
    const changeAge = (
        e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
        seatNo: string
    ) => dispatch(setPassengerAge({ seatNo, age: e.target.value as string }))
    const changeGender = (e: SelectChangeEvent<string>, seatNo: string) =>
        dispatch(setPassengerGender({ seatNo, gender: e.target.value }))
    return (
        <>
            {bookSeatDetails?.map((seat, idx) => {
                return (
                    <Box
                        component="div"
                        sx={{
                            '& .MuiTextField-root': { m: 1, width: '25ch' },
                        }}
                        key={idx}
                    >
                        <h5>
                            <b>SeatNo: {seat.seatNo}</b>
                        </h5>
                        <div className="flex flex-col justify-between gap-3 m-1 p-5 sm:flex-row">
                            <TextField
                                required
                                name="name"
                                label="Passenger name"
                                value={seat.passenger.name}
                                onChange={(e) => changeName(e, seat.seatNo)}
                            />
                            <TextField
                                label="Age"
                                name="age"
                                type="number"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={seat.passenger.age}
                                onChange={(e) => changeAge(e, seat.seatNo)}
                            />
                            <FormControl sx={{ margin: '8px' }}>
                                <InputLabel id="type-select-label">
                                    Type
                                </InputLabel>
                                <Select
                                    labelId="type-select-label"
                                    id="type"
                                    name="type"
                                    label="Type"
                                    value={seat.passenger.gender}
                                    onChange={(e) =>
                                        changeGender(e, seat.seatNo)
                                    }
                                >
                                    <MenuItem value={'Male'}>Male</MenuItem>
                                    <MenuItem value={'Female'}>Female</MenuItem>
                                    <MenuItem value={'Other'}>Other</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                    </Box>
                )
            })}
        </>
    )
}
