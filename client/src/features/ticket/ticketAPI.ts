import api from '../../utils/api'
import { TicketBody } from './ticketInterface'

export async function postTicketApi(
    tripId: string | undefined,
    body: TicketBody[] | null
) {
    const { data } = await api.post(`/ticket/${tripId}`, {
        seats: body,
    })
    return data
}
