import { Passenger } from '../bookings/bookingInterface'
import type { Error } from '../../components/alert/alertInterface'

export interface TicketBody {
    seatNo: string
    passenger: Passenger
}

export interface TicketState {
    bookSeatDetails: TicketBody[] | null
    ticketId: string | null
    isLoading: boolean
    errors: Error[] | null
}
