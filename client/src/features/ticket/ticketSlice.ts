import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AxiosError } from 'axios'
import { RootState } from '../../app/store'
import type { Error } from '../../components/alert/alertInterface'
import { postTicketApi } from './ticketAPI'
import { TicketState } from './ticketInterface'

const initialState: TicketState = {
    bookSeatDetails: null,
    ticketId: null,
    isLoading: false,
    errors: null,
}

export const postTicket = createAsyncThunk<
    string | Error[],
    string | undefined,
    { state: RootState }
>(
    'ticket/book',
    async (tripId: string | undefined, { getState, rejectWithValue }) => {
        try {
            const state = getState()
            const data = await postTicketApi(
                tripId,
                state.ticket.bookSeatDetails
            )
            return data
        } catch (err: unknown) {
            if (!(err instanceof AxiosError) || !err.response) {
                throw err
            }
            return rejectWithValue(err.response.data.errors)
        }
    }
)

export const ticketSlice = createSlice({
    name: 'bookings',
    initialState,
    reducers: {
        addSeatNo: (state, action: PayloadAction<string>) => {
            if (!state.bookSeatDetails) state.bookSeatDetails = []
            state.bookSeatDetails.push({
                seatNo: action.payload,
                passenger: {
                    name: '',
                    age: 1,
                    gender: 'Male',
                },
            })
        },
        removeSeatNo: (state, action: PayloadAction<string>) => {
            if (state.bookSeatDetails)
                state.bookSeatDetails = state.bookSeatDetails.filter(
                    (seat) => seat.seatNo !== action.payload
                )
        },
        setPassengerName: (
            state,
            action: PayloadAction<{ seatNo: string; name: string }>
        ) => {
            if (state.bookSeatDetails)
                state.bookSeatDetails = state.bookSeatDetails?.map((seat) => {
                    const { seatNo, name } = action.payload
                    if (seat.seatNo === seatNo) seat.passenger.name = name
                    return seat
                })
        },
        setPassengerAge: (
            state,
            action: PayloadAction<{ seatNo: string; age: string }>
        ) => {
            if (state.bookSeatDetails)
                state.bookSeatDetails = state.bookSeatDetails?.map((seat) => {
                    const { seatNo, age } = action.payload
                    if (seat.seatNo === seatNo)
                        seat.passenger.age = parseInt(age)
                    return seat
                })
        },
        setPassengerGender: (
            state,
            action: PayloadAction<{ seatNo: string; gender: string }>
        ) => {
            if (state.bookSeatDetails)
                state.bookSeatDetails = state.bookSeatDetails?.map((seat) => {
                    const { seatNo, gender } = action.payload
                    if (seat.seatNo === seatNo) seat.passenger.gender = gender
                    return seat
                })
        },
        resetTicket: () => {
            return initialState
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(postTicket.pending, (state) => {
                state.isLoading = true
                state.ticketId = null
                state.errors = null
            })
            .addCase(postTicket.fulfilled, (state, action) => {
                state.isLoading = false
                state.ticketId = action.payload as string
            })
            .addCase(postTicket.rejected, (state, action) => {
                state.isLoading = false
                state.errors = action.payload as Error[]
            })
    },
})

export const {
    removeSeatNo,
    addSeatNo,
    setPassengerName,
    setPassengerAge,
    setPassengerGender,
    resetTicket,
} = ticketSlice.actions

export const selectTicket = (state: RootState) => state.ticket

export default ticketSlice.reducer
