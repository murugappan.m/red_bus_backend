import { Document } from 'mongoose'
import { connectDB, disconnectDB } from '../src/config/db'
import { busError } from '../src/errorResponse'
import { Bus, User } from '../src/interface'
import {
    busSeater,
    busSleeper,
    busSleeperSeaterAc,
    registerBus,
} from '../seed/bus.seed'
import { trip } from '../seed/trip.seed'
import { customer1, operator1, operator2 } from '../seed/user.seed'
import { createBus } from '../src/services/bus.service'
import { createTrip } from '../src/services/trip.service'
import { createToken, createUser } from '../src/services/user.service'
import {
    BAD_REQUEST,
    CREATION_SUCCESSFULL,
    OK,
    NOT_FOUND,
    NO_CONTENT,
} from '../src/statusCodes'
import { request } from './app.test'
import { wrongId, wrongRole } from './common/role'

describe('/api/bus', () => {
    let op1: Document & User,
        op2,
        cus1,
        bus1: Document & Bus,
        bus2: Document & Bus,
        cus1Token: string,
        op2Token: string,
        op1Token: string
    const route = '/api/bus'
    beforeAll(async () => {
        await connectDB()
        op1 = await createUser(
            operator1.email,
            operator1.name,
            operator1.password,
            operator1.type
        )
        op2 = await createUser(
            operator2.email,
            operator2.name,
            operator2.password,
            operator2.type
        )
        cus1 = await createUser(
            customer1.email,
            customer1.name,
            customer1.password,
            customer1.type
        )
        cus1Token = createToken({ _id: cus1._id.toString(), type: cus1.type })
        op1Token = createToken({ _id: op1._id.toString(), type: op1.type })
        op2Token = createToken({ _id: op2._id.toString(), type: op2.type })
        bus1 = await createBus(
            op1._id.toString(),
            busSleeper.busNo,
            busSleeper.ac,
            busSleeper.type
        )
        bus2 = await createBus(
            op2._id.toString(),
            busSleeperSeaterAc.busNo,
            busSleeperSeaterAc.ac,
            busSleeperSeaterAc.type
        )
        // trip in future for bus 1
        await createTrip(
            bus1,
            trip.from,
            trip.to,
            trip.startDateTime,
            trip.endDateTime,
            trip.sleeperPrice,
            trip.seaterPrice,
            trip.singlePriceMarkup
        )
        // trip in past for bus 2
        await createTrip(
            bus2,
            trip.from,
            trip.to,
            '1997-10-21T10:08:54Z',
            '1997-10-21T10:08:54Z',
            trip.sleeperPrice,
            trip.seaterPrice,
            trip.singlePriceMarkup
        )
    })
    afterAll(async () => {
        await disconnectDB()
    })
    describe('POST', () => {
        it('wrong role', async () => {
            wrongRole(await request.post(route).set('x-auth-token', cus1Token))
        })
        it('bus no already registered', async () => {
            const res = await request
                .post(route)
                .set('x-auth-token', op2Token)
                .send({
                    busNo: bus1.busNo,
                    ac: true,
                    sleeper: true,
                    seater: true,
                })
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(busError.registerNoRepeated)
        })
        it('unique bus', async () => {
            const res = await request
                .post(route)
                .set('x-auth-token', op2Token)
                .send(registerBus)
            expect(res.status).toEqual(CREATION_SUCCESSFULL)
            expect(res.body).toBeDefined()
        })
    })

    describe('GET', () => {
        it('invalid role', async () => {
            wrongRole(await request.get(route).set('x-auth-token', cus1Token))
        })
        it('valid role', async () => {
            const res = await request.get(route).set('x-auth-token', op1Token)
            expect(res.status).toEqual(OK)
            expect(res.body).toEqual([
                {
                    _id: bus1._id.toString(),
                    operatorId: bus1.operatorId.toString(),
                    busNo: bus1.busNo,
                    type: bus1.type,
                    ac: bus1.ac,
                },
            ])
        })
    })
    describe('GET /:_id', () => {
        it('invalid role', async () => {
            wrongRole(
                await request
                    .get(route + `/${bus1._id.toString()}`)
                    .set('x-auth-token', cus1Token)
            )
        })
        it('incorrect bus id', async () => {
            wrongId(
                await request.get(route + '/1234').set('x-auth-token', op1Token)
            )
        })
        it('bus registered by different operator', async () => {
            const res = await request
                .get(route + `/${bus1._id.toString()}`)
                .set('x-auth-token', op2Token)
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(busError.notFound)
        })
        it('valid id with correct user id', async () => {
            const res = await request
                .get(route + `/${bus1._id.toString()}`)
                .set('x-auth-token', op1Token)
            expect(res.status).toEqual(OK)
            expect(res.body).toEqual({
                _id: bus1._id.toString(),
                operatorId: bus1.operatorId.toString(),
                busNo: bus1.busNo,
                ac: bus1.ac,
                type: bus1.type,
            })
        })
    })
    describe('PUT', () => {
        const errors = [
            {
                msg: 'specify whether bus is ac or not',
                param: 'ac',
                location: 'body',
            },
            {
                msg: 'specify type of bus',
                param: 'type',
                location: 'body',
            },
            {
                msg: 'type must be one of Sleeper, Seater or SeaterSleeper',
                param: 'type',
                location: 'body',
            },
        ]
        it('invalid role', async () => {
            wrongRole(
                await request
                    .put(route + `/${bus1._id.toString()}`)
                    .set('x-auth-token', cus1Token)
            )
        })
        it('incorrect bus id', async () => {
            wrongId(
                await request.put(route + '/1234').set('x-auth-token', op1Token)
            )
        })
        it('bus registered by different operator', async () => {
            const res = await request
                .put(route + `/${bus1._id.toString()}`)
                .set('x-auth-token', op2Token)
                .send(busSleeper)
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(busError.notFound)
        })
        it('valid id with invalid fields', async () => {
            const res = await request
                .put(route + `/${bus2._id.toString()}`)
                .set('x-auth-token', op2Token)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors).toEqual(errors)
        })
        it('valid id with crct fields but trip registered in future', async () => {
            const res = await request
                .put(route + `/${bus1._id.toString()}`)
                .set('x-auth-token', op1Token)
                .send(busSeater)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(busError.tripInFuture)
        })
        it('valid id with crct fields and trip registered in past/no trips', async () => {
            const res = await request
                .put(route + `/${bus2._id.toString()}`)
                .set('x-auth-token', op2Token)
                .send(busSeater)
            expect(res.status).toEqual(NO_CONTENT)
        })
    })
    describe('DELETE', () => {
        it('invalid role', async () => {
            wrongRole(
                await request
                    .delete(route + `/${bus1._id.toString()}`)
                    .set('x-auth-token', cus1Token)
            )
        })
        it('incorrect bus id', async () => {
            wrongId(
                await request
                    .delete(route + '/1234')
                    .set('x-auth-token', op1Token)
            )
        })
        it('bus registered by different operator', async () => {
            const res = await request
                .delete(route + `/${bus1._id.toString()}`)
                .set('x-auth-token', op2Token)
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(busError.notFound)
        })
        it('valid id but trip registered in future', async () => {
            const res = await request
                .delete(route + `/${bus1._id.toString()}`)
                .set('x-auth-token', op1Token)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(busError.tripInFuture)
        })
        it('valid id and trip registered in past/no trips', async () => {
            const res = await request
                .delete(route + `/${bus2._id.toString()}`)
                .set('x-auth-token', op2Token)
            expect(res.status).toEqual(NO_CONTENT)
        })
    })
})
