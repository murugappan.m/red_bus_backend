import { Document, Types } from 'mongoose'
import { connectDB, disconnectDB } from '../src/config/db'
import { ticketError, tripError } from '../src/errorResponse'
import { Trip, User } from '../src/interface'
import { busSleeper } from '../seed/bus.seed'
import {
    repeatedSeatsRequest,
    ticketRegisterSeedPost,
    invalidTicketSeed,
    ticketSeed,
    ticketRegisterSeedPut,
    ticketSeedSameSeat,
} from '../seed/ticket.seed'
import { trip } from '../seed/trip.seed'
import { customer1, customer2, operator1 } from '../seed/user.seed'
import { createBus } from '../src/services/bus.service'
import {
    bookTicket,
    createTicket,
    getTicketOfIds,
} from '../src/services/ticket.service'
import { createTrip } from '../src/services/trip.service'
import { createToken, createUser } from '../src/services/user.service'
import {
    BAD_REQUEST,
    CREATION_SUCCESSFULL,
    NOT_FOUND,
    OK,
    NO_CONTENT,
} from '../src/statusCodes'
import { request } from './app.test'
import { wrongId, wrongRole } from './common/role'

describe('/api/ticket', () => {
    let cus1: Document<unknown, any, User> & User & { _id: Types.ObjectId },
        cus2,
        op,
        bus,
        tripPast: Document<any, any, any> & Trip,
        tripFuture: Document<any, any, any> & Trip,
        ticketFutureId: string,
        ticketPastId: string,
        cus1Token: string,
        cus2Token: string,
        opToken: string
    const route = '/api/ticket'

    beforeAll(async () => {
        await connectDB()
        cus1 = await createUser(
            customer1.email,
            customer1.name,
            customer1.password,
            customer1.type
        )
        cus2 = await createUser(
            customer2.email,
            customer2.name,
            customer2.password,
            customer2.type
        )
        op = await createUser(
            operator1.email,
            operator1.name,
            operator1.password,
            operator1.type
        )
        cus1Token = createToken({ _id: cus1._id.toString(), type: cus1.type })
        cus2Token = createToken({ _id: cus2._id.toString(), type: cus2.type })
        opToken = createToken({ _id: op._id.toString(), type: op.type })
        bus = await createBus(
            op._id.toString(),
            busSleeper.busNo,
            busSleeper.ac,
            busSleeper.type
        )
        tripPast = await createTrip(
            bus,
            trip.from,
            trip.to,
            '1997-10-21T10:08:54Z',
            '1997-10-21T11:08:54Z',
            trip.sleeperPrice,
            trip.seaterPrice,
            trip.singlePriceMarkup
        )
        tripFuture = await createTrip(
            bus,
            trip.from,
            trip.to,
            '2025-10-21T10:08:54Z',
            '2025-10-21T16:08:54Z',
            trip.sleeperPrice,
            trip.seaterPrice,
            trip.singlePriceMarkup
        )
        let ticket = createTicket(
            ticketSeed.seats,
            tripFuture,
            cus1._id.toString()
        )
        await bookTicket(tripFuture._id, ticket)
        ticketFutureId = ticket._id.toString()
        ticket = createTicket(ticketSeed.seats, tripPast, cus1._id.toString())
        await bookTicket(tripPast._id, ticket)
        ticketPastId = ticket._id.toString()
    })
    afterAll(disconnectDB)
    describe('POST', () => {
        it('wrong role', async () => {
            wrongRole(
                await request
                    .post(route + `/${tripFuture._id.toString()}`)
                    .set('x-auth-token', opToken)
            )
        })
        it('invalid trip id', async () => {
            wrongId(
                await request
                    .post(route + '/1234')
                    .set('x-auth-token', cus1Token)
            )
        })
        it('trip id in past', async () => {
            const res = await request
                .post(route + `/${tripPast._id.toString()}`)
                .set('x-auth-token', cus1Token)
                .send(invalidTicketSeed)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.inPast)
        })
        it('invalid fields(seats)', async () => {
            const res = await request
                .post(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', cus1Token)
                .send({
                    seats: [],
                })
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(
                'seats must be array with atleast one seat details'
            )
        })
        it('seats already booked', async () => {
            const res = await request
                .post(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', cus1Token)
                .send(ticketSeed)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(ticketError.booked)
        })
        it('same seat no repeated', async () => {
            const res = await request
                .post(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', cus1Token)
                .send(repeatedSeatsRequest)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(`seat number repeated`)
        })
        it('correct trip id, seats not booked', async () => {
            const res = await request
                .post(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', cus1Token)
                .send(ticketRegisterSeedPost)
            expect(res.status).toEqual(CREATION_SUCCESSFULL)
            expect(res.text).toBeDefined()
        })
    })

    describe('GET', () => {
        it('wrong role', async () => {
            wrongRole(await request.get(route).set('x-auth-token', opToken))
        })
        it('no tickets', async () => {
            const res = await request.get(route).set('x-auth-token', cus2Token)
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(ticketError.empty)
        })
        it('tickets booked', async () => {
            const res = await request.get(route).set('x-auth-token', cus1Token)
            expect(res.status).toEqual(OK)
            expect(Array.isArray(res.body)).toBe(true)
        })
    })
    describe('GET /:_id', () => {
        it('wrong role', async () => {
            wrongRole(
                await request
                    .get(route + `/${ticketFutureId.toString()}`)
                    .set('x-auth-token', opToken)
            )
        })
        it('incorrect ticket id', async () => {
            wrongId(
                await request
                    .get(route + '/1234')
                    .set('x-auth-token', cus1Token)
            )
        })
        it('ticket booked by different customer', async () => {
            const res = await request
                .get(route + `/${ticketFutureId.toString()}`)
                .set('x-auth-token', cus2Token)
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(ticketError.notFound)
        })
        it('valid id with correct user id', async () => {
            const res = await request
                .get(route + `/${ticketFutureId.toString()}`)
                .set('x-auth-token', cus1Token)
            expect(res.status).toEqual(OK)
            // some issue with seats map object conversion, hence not able to test seats response
            const procTicket = await getTicketOfIds(
                ticketFutureId,
                cus1._id.toString()
            )
            const expectedBody = {
                from: procTicket?.from,
                to: procTicket?.to,
                startDateTime: procTicket?.startDateTime?.toISOString(),
                endDateTime: procTicket?.endDateTime?.toISOString(),
            }
            expect(res.body).toMatchObject(expectedBody)
        })
    })
    describe('PUT', () => {
        it('wrong role', async () => {
            wrongRole(
                await request
                    .put(route + `/${ticketFutureId}`)
                    .set('x-auth-token', opToken)
            )
        })
        it('incorrect ticket id', async () => {
            wrongId(
                await request
                    .put(route + '/1234')
                    .set('x-auth-token', cus1Token)
                    .send(ticketRegisterSeedPut)
            )
        })
        it('trip id in past', async () => {
            const res = await request
                .put(route + `/${ticketPastId}`)
                .set('x-auth-token', cus1Token)
                .send(ticketRegisterSeedPut)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.inPast)
        })
        it('invalid fields(seats)', async () => {
            const res = await request
                .put(route + `/${ticketFutureId}`)
                .set('x-auth-token', cus1Token)
                .send(invalidTicketSeed)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(
                `seatNo ${invalidTicketSeed.seats[0].seatNo} does not exist`
            )
        })
        it('seats already booked by different ticket', async () => {
            const res = await request
                .put(route + `/${ticketFutureId}`)
                .set('x-auth-token', cus1Token)
                .send(ticketRegisterSeedPost)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(ticketError.booked)
        })
        it('same seat no repeated', async () => {
            const res = await request
                .put(route + `/${ticketFutureId}`)
                .set('x-auth-token', cus1Token)
                .send(repeatedSeatsRequest)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(`seat number repeated`)
        })
        it('correct trip id, seats booked by same ticket', async () => {
            const res = await request
                .put(route + `/${ticketFutureId}`)
                .set('x-auth-token', cus1Token)
                .send(ticketSeedSameSeat)
            expect(res.status).toEqual(NO_CONTENT)
        })
        it('correct trip id, seats not booked', async () => {
            const res = await request
                .put(route + `/${ticketFutureId}`)
                .set('x-auth-token', cus1Token)
                .send(ticketRegisterSeedPut)
            expect(res.status).toEqual(NO_CONTENT)
        })
    })
    describe('DELETE', () => {
        it('wrong role', async () => {
            wrongRole(
                await request
                    .delete(route + `/${ticketFutureId}`)
                    .set('x-auth-token', opToken)
            )
        })
        it('incorrect ticket id', async () => {
            wrongId(
                await request
                    .delete(route + '/1234')
                    .set('x-auth-token', cus1Token)
            )
        })
        it('ticket booked by different customer', async () => {
            const res = await request
                .delete(route + `/${ticketFutureId}`)
                .set('x-auth-token', cus2Token)
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(ticketError.notFound)
        })
        it('valid id but trip completed', async () => {
            const res = await request
                .delete(route + `/${ticketPastId}`)
                .set('x-auth-token', cus1Token)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.inPast)
        })
        it('valid id, trip in future', async () => {
            const res = await request
                .delete(route + `/${ticketFutureId}`)
                .set('x-auth-token', cus1Token)
            expect(res.status).toEqual(NO_CONTENT)
        })
    })
})
