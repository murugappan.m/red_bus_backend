import { connectDB, disconnectDB } from '../src/config/db'
import { request } from './app.test'
import { busSeater, busSleeper, busSleeperSeaterAc } from '../seed/bus.seed'
import { trip } from '../seed/trip.seed'
import { operator1, operator2, customer1 } from '../seed/user.seed'
import { createBus } from '../src/services/bus.service'
import { createTrip, getSeats } from '../src/services/trip.service'
import { createUser, createToken } from '../src/services/user.service'
import { wrongId, wrongRole } from './common/role'
import { Bus, Trip } from '../src/interface'
import { Document, Types } from 'mongoose'
import { bookTicket, createTicket } from '../src/services/ticket.service'
import { ticketSeed } from '../seed/ticket.seed'
import {
    NOT_FOUND,
    BAD_REQUEST,
    CREATION_SUCCESSFULL,
    OK,
    NO_CONTENT,
} from '../src/statusCodes'
import { busError, tripError } from '../src/errorResponse'

describe('/api/trip', () => {
    let op1,
        op2,
        cus1,
        bus1: Document & Bus,
        bus2: Document & Bus,
        bus3: Document & Bus,
        cus1Token: string,
        op2Token: string,
        op1Token: string,
        tripFuture: Document<unknown, any, Trip> &
            Trip & { _id: Types.ObjectId },
        tripPast: Document<unknown, any, Trip> & Trip & { _id: Types.ObjectId },
        tripWithSeats: Document<unknown, any, Trip> &
            Trip & { _id: Types.ObjectId }
    const route = '/api/trip'
    beforeAll(async () => {
        await connectDB()
        op1 = await createUser(
            operator1.email,
            operator1.name,
            operator1.password,
            operator1.type
        )
        op2 = await createUser(
            operator2.email,
            operator2.name,
            operator2.password,
            operator2.type
        )
        cus1 = await createUser(
            customer1.email,
            customer1.name,
            customer1.password,
            customer1.type
        )
        cus1Token = createToken({ _id: cus1._id.toString(), type: cus1.type })
        op1Token = createToken({ _id: op1._id.toString(), type: op1.type })
        op2Token = createToken({ _id: op2._id.toString(), type: op2.type })
        bus1 = await createBus(
            op1._id.toString(),
            busSleeper.busNo,
            busSleeper.ac,
            busSleeper.type
        )
        bus2 = await createBus(
            op2._id.toString(),
            busSleeperSeaterAc.busNo,
            busSleeperSeaterAc.ac,
            busSleeperSeaterAc.type
        )
        bus3 = await createBus(
            op1._id.toString(),
            busSeater.busNo,
            busSeater.ac,
            busSeater.type
        )
        // trip in past for bus 2
        tripPast = await createTrip(
            bus2,
            trip.from,
            trip.to,
            '1997-10-21T10:08:54Z',
            '1997-10-21T11:08:54Z',
            trip.sleeperPrice,
            trip.seaterPrice,
            trip.singlePriceMarkup
        )
        tripFuture = await createTrip(
            bus1,
            trip.from,
            trip.to,
            '2025-10-21T10:08:54Z',
            '2025-10-21T16:08:54Z',
            trip.sleeperPrice,
            trip.seaterPrice,
            trip.singlePriceMarkup
        )
        tripWithSeats = await createTrip(
            bus1,
            trip.from,
            trip.to,
            '2027-10-21T10:08:54Z',
            '2027-10-21T16:08:54Z',
            trip.sleeperPrice,
            trip.seaterPrice,
            trip.singlePriceMarkup
        )
        await bookTicket(
            tripWithSeats._id,
            createTicket(ticketSeed.seats, tripWithSeats, cus1._id.toString())
        )
    })
    afterAll(async () => {
        await disconnectDB()
    })
    describe('POST', () => {
        it('wrong role', async () => {
            wrongRole(await request.post(route).set('x-auth-token', cus1Token))
        })
        it('invalid bus id', async () => {
            wrongId(
                await request
                    .post(route)
                    .set('x-auth-token', op1Token)
                    .send({ busId: '1234' })
            )
        })
        it('bus id not linked to operator', async () => {
            const res = await request
                .post(route)
                .set('x-auth-token', op2Token)
                .send({ busId: bus1._id.toString(), ...trip })
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(busError.notFound)
        })
        it('end date < start date', async () => {
            const res = await request
                .post(route)
                .set('x-auth-token', op1Token)
                .send({
                    busId: bus1._id.toString(),
                    ...trip,
                    startDateTime: '2022-10-21T11:08:55Z',
                    endDateTime: '2022-10-21T10:08:55Z',
                })
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0]).toEqual({
                value: '2022-10-21T10:08:55Z',
                msg: 'endDate must be greater than start date',
                param: 'endDateTime',
                location: 'body',
            })
        })
        it('end date or start date in past', async () => {
            const res = await request
                .post(route)
                .set('x-auth-token', op1Token)
                .send({
                    busId: bus1._id.toString(),
                    ...trip,
                    startDateTime: '1997-10-21T10:08:55Z',
                    endDateTime: '1997-10-21T11:08:55Z',
                })
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors).toEqual([
                {
                    value: '1997-10-21T10:08:55Z',
                    msg: 'Date must be greater than today',
                    param: 'startDateTime',
                    location: 'body',
                },
                {
                    value: '1997-10-21T11:08:55Z',
                    msg: 'Date must be greater than today',
                    param: 'endDateTime',
                    location: 'body',
                },
            ])
        })
        it('bus linked to operator but trip exists on given dates', async () => {
            const res = await request
                .post(route)
                .set('x-auth-token', op1Token)
                .send({
                    busId: bus1._id.toString(),
                    ...trip,
                    startDateTime: '2025-10-21T11:08:55Z',
                    endDateTime: '2025-10-21T17:08:55Z',
                })
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.intervalClash)
        })
        it('bus linked to operator and no trip in timeframe', async () => {
            const res = await request
                .post(route)
                .set('x-auth-token', op2Token)
                .send({
                    busId: bus2._id.toString(),
                    ...trip,
                    startDateTime: '2025-10-21T11:08:55Z',
                    endDateTime: '2025-10-21T17:08:55Z',
                })
            expect(res.status).toEqual(CREATION_SUCCESSFULL)
            expect(res.text).toBeDefined()
        })
    })

    describe('GET', () => {
        it('wrong role', async () => {
            wrongRole(await request.get(route).set('x-auth-token', cus1Token))
        })
        it('invalid bus id', async () => {
            wrongId(
                await request
                    .get(route)
                    .set('x-auth-token', op1Token)
                    .query({ busId: '1234' })
            )
        })
        it('bus id not linked to operator', async () => {
            const res = await request
                .get(route)
                .set('x-auth-token', op1Token)
                .query({ busId: bus2._id.toString() })
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(tripError.empty)
        })
        it('bus linked to operator and no trip', async () => {
            const res = await request
                .get(route)
                .set('x-auth-token', op1Token)
                .query({ busId: bus3._id.toString() })
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(tripError.empty)
        })
        it('bus linked to operator and trips available', async () => {
            const res = await request
                .get(route)
                .set('x-auth-token', op2Token)
                .query({ busId: bus2._id.toString() })
            expect(res.status).toEqual(OK)
            expect(res.body).toEqual(expect.arrayContaining([]))
            // note: not able to test structure of trip because of map inconsistency
        })
    })
    describe('GET /:_id', () => {
        it('incorrect trip id', async () => {
            wrongId(
                await request.get(route + '/1234').set('x-auth-token', op1Token)
            )
        })
        it('trip booked by different operator', async () => {
            const res = await request
                .get(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', op2Token)
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(tripError.notFound)
        })
        it('valid trip id with crct operator token', async () => {
            const res = await request
                .get(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', op1Token)
            expect(res.status).toEqual(OK)
            expect(res.body).toBeDefined()
        })
    })
    describe('PUT', () => {
        it('wrong role', async () => {
            wrongRole(
                await request
                    .put(route + '/1234')
                    .set('x-auth-token', cus1Token)
            )
        })
        it('incorrect trip id', async () => {
            wrongId(
                await request
                    .put(route + '/1234')
                    .set('x-auth-token', op1Token)
                    .send({ busId: bus1._id.toString(), ...trip })
            )
        })
        it('trip created by different operator', async () => {
            const res = await request
                .put(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', op2Token)
                .send({ busId: bus1._id.toString(), ...trip })
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(tripError.notFound)
        })
        it('valid id with crct fields but trip has seats booked', async () => {
            const res = await request
                .put(route + `/${tripWithSeats._id.toString()}`)
                .set('x-auth-token', op1Token)
                .send({
                    busId: bus1._id.toString(),
                    ...trip,
                    startDateTime: '2025-10-24T10:08:54Z',
                    endDateTime: '2025-10-24T16:08:54Z',
                })
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.ticketBooked)
        })
        it('valid id with crct fields but trip registered in past', async () => {
            const res = await request
                .put(route + `/${tripPast._id.toString()}`)
                .set('x-auth-token', op2Token)
                .send({ busId: bus1._id.toString(), ...trip })
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.inPast)
        })
        it('valid id with crct fields and no bookings', async () => {
            const res = await request
                .put(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', op1Token)
                .send({ busId: bus1._id.toString(), ...trip })
            expect(res.status).toEqual(NO_CONTENT)
        })
    })
    describe('DELETE', () => {
        it('wrong role', async () => {
            wrongRole(
                await request
                    .delete(route + '/1234')
                    .set('x-auth-token', cus1Token)
            )
        })
        it('invalid trip id', async () => {
            wrongId(
                await request
                    .delete(route + '/1234')
                    .set('x-auth-token', op1Token)
                    .send()
            )
        })
        it('trip id with incorrect user id', async () => {
            const res = await request
                .delete(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', op2Token)
            expect(res.status).toEqual(NOT_FOUND)
            expect(res.body.errors[0].msg).toEqual(tripError.notFound)
        })
        it('valid id but trip registered in past', async () => {
            const res = await request
                .delete(route + `/${tripPast._id.toString()}`)
                .set('x-auth-token', op2Token)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.inPast)
        })
        it('valid id but trip has seats booked', async () => {
            const res = await request
                .delete(route + `/${tripWithSeats._id.toString()}`)
                .set('x-auth-token', op1Token)
            expect(res.status).toEqual(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.ticketBooked)
        })
        it('valid id, no bookings, trip in future', async () => {
            const res = await request
                .delete(route + `/${tripFuture._id.toString()}`)
                .set('x-auth-token', op1Token)
            expect(res.status).toEqual(NO_CONTENT)
            // recreate for remaining two tests
            tripFuture = await createTrip(
                bus1,
                trip.from,
                trip.to,
                '2025-10-21T10:08:54Z',
                '2025-10-21T16:08:54Z',
                trip.sleeperPrice,
                trip.seaterPrice,
                trip.singlePriceMarkup
            )
        })
    })

    describe('GET /api/searchTrip', () => {
        it('wrong role', async () => {
            wrongRole(
                await request
                    .get('/api/searchTrip')
                    .set('x-auth-token', op1Token)
                    .send()
            )
        })
        it('incorrect request body', async () => {
            const res = await request
                .get('/api/searchTrip')
                .set('x-auth-token', cus1Token)
                .send()
            expect(res.status).toBe(BAD_REQUEST)
            expect(res.body.errors).toBeDefined()
        })
        it('date in past', async () => {
            const res = await request
                .get('/api/searchTrip')
                .set('x-auth-token', cus1Token)
                .query({
                    from: 'madurai',
                    to: 'coimbatore',
                    date: '1997-10-21T10:08:54Z',
                    ac: false,
                })
            expect(res.status).toBe(BAD_REQUEST)
            expect(res.body.errors).toBeDefined()
        })
        it('date in future and no trips available', async () => {
            const res = await request
                .get('/api/searchTrip')
                .set('x-auth-token', cus1Token)
                .query({
                    from: 'madurai',
                    to: 'coimbatore',
                    date: '2025-11-21T10:08:54Z',
                })
            expect(res.status).toBe(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(tripError.notFoundSearch)
        })
        it('date in future and trips available', async () => {
            const res = await request
                .get('/api/searchTrip')
                .set('x-auth-token', cus1Token)
                .query({
                    from: 'madurai',
                    to: 'coimbatore',
                    date: '2025-10-21T10:08:54Z',
                    ac: false,
                })
            expect(res.status).toBe(OK)
            expect(res.body.length).toBeGreaterThan(0)
        })
    })

    describe('GET /api/seats', () => {
        it('incorrect trip id', async () => {
            wrongId(
                await request
                    .get('/api/seats/1234')
                    .set('x-auth-token', cus1Token)
            )
        })
        it('valid trip id', async () => {
            const id = tripPast._id
            const res = await request
                .get(`/api/seats/${id.toString()}`)
                .set('x-auth-token', cus1Token)
            expect(res.status).toBe(OK)
            expect(res.body).toEqual(await getSeats(id))
        })
    })
})
