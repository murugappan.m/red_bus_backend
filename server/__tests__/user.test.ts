import { Document } from 'mongoose'
import { connectDB, disconnectDB } from '../src/config/db'
import { createToken, createUser } from '../src/services/user.service'
import { request } from './app.test'
import { customer1, customer2 } from '../seed/user.seed'
import {
    CREATION_SUCCESSFULL,
    BAD_REQUEST,
    OK,
    INVALID_TOKEN,
} from '../src/statusCodes'
import { tokenError, userError } from '../src/errorResponse'
import { User } from '../src/interface'

describe('POST /api/user', () => {
    beforeAll(async () => {
        await connectDB()
    })
    afterAll(async () => {
        await disconnectDB()
    })
    it('correct details', async () => {
        const body = customer2
        const res = await request
            .post('/api/user')
            .set('Content-type', 'application/json')
            .send(body)
        expect(res.status).toBe(CREATION_SUCCESSFULL)
        expect(res.body).toBeDefined()
    })
    it('no user details in body', async () => {
        const res = await request
            .post('/api/user')
            .set('Content-type', 'application/json')
            .send()
        expect(res.status).toBe(BAD_REQUEST)
        expect(res.body.errors).toBeInstanceOf(Array)
    })
})

describe('/api/auth', () => {
    let jwtToken: string
    let user: Document & User
    const { email, name, password, type } = customer1
    beforeAll(async () => {
        await connectDB()
        try {
            user = await createUser(email, name, password, type)
            jwtToken = createToken({
                _id: user._id.toString(),
                type: user.type,
            })
        } catch (err) {
            console.error(err)
            process.exit(1)
        }
    })
    afterAll(async () => {
        await disconnectDB()
    })
    describe('POST', () => {
        it('correct email & password', async () => {
            const body = {
                email: user.email,
                password: password,
            }
            const res = await request
                .post('/api/auth')
                .set('Content-type', 'application/json')
                .send(body)
            expect(res.status).toBe(CREATION_SUCCESSFULL)
            expect(res.body).toBeDefined()
        })
        it('incorrect password', async () => {
            const body = {
                email: user.email,
                password: 'dasdas',
            }
            const res = await request
                .post('/api/auth')
                .set('Content-type', 'application/json')
                .send(body)
            expect(res.status).toBe(BAD_REQUEST)
            expect(res.body.errors[0].msg).toEqual(userError.invalid)
        })
    })
    describe('GET', () => {
        it('correct details', async () => {
            const res = await request
                .get('/api/auth')
                .set('x-auth-token', jwtToken)
                .send()
            const { email, name, type } = res.body
            expect(res.status).toBe(OK)
            expect(email).toEqual(user.email)
            expect(name).toEqual(user.name)
            expect(type).toEqual(user.type)
        })
        it('invalid token', async () => {
            const res = await request
                .get('/api/auth')
                .set('x-auth-token', '1234')
                .send()
            expect(res.status).toBe(INVALID_TOKEN)
            expect(res.body.errors[0].msg).toEqual(tokenError.invalid)
        })
    })
})
