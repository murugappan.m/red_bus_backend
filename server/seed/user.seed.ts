export const customer1 = {
    email: 'customer1@gmail.com',
    name: 'customer1',
    type: 'Customer',
    password: 'Customer@123',
}

export const customer2 = {
    email: 'customer2@gmail.com',
    name: 'customer2',
    type: 'Customer',
    password: 'Customer@123',
}

export const operator1 = {
    email: 'operator1@gmail.com',
    name: 'operator1',
    type: 'Operator',
    password: 'Operator@123',
}

export const operator2 = {
    email: 'operator2@gmail.com',
    name: 'operator2',
    type: 'Operator',
    password: 'Operator@123',
}
