import express, { Express, json } from 'express'
import userRouter from './routes/api/user.route'
import loginRouter from './routes/api/auth.route'
import searchRouter from './routes/api/search.route'
import seatRouter from './routes/api/seat.route'
import tripRouter from './routes/api/trip.route'
import ticketRouter from './routes/api/ticket.route'
import busRouter from './routes/api/bus.route'
import cors from 'cors'
import {
    handleDatabaseError,
    handleDefaultError,
    handleMongooseError,
    handleValidationError,
} from './middleware/errorHandler.middleware'
import { OK } from './statusCodes'
import path from 'path'
const app: Express = express()

// Cors Configuration - to allow swagger ui to read response
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested, Content-Type, Accept Authorization'
    )
    if (req.method === 'OPTIONS') {
        res.header(
            'Access-Control-Allow-Methods',
            'POST, PUT, PATCH, GET, DELETE'
        )
        return res.status(OK).json({})
    }
    next()
})

// cors for development
if (process.env.NODE_ENV === 'development') app.use(cors())

// init middleware for express validator to be able to intercept request
app.use(json())

// Define Routes
app.use('/api/user', userRouter)
app.use('/api/auth', loginRouter)
app.use('/api/bus', busRouter)
app.use('/api/trip', tripRouter)
app.use('/api/searchTrip', searchRouter)
app.use('/api/seats', seatRouter)
app.use('/api/ticket', ticketRouter)

// error handlers
app.use(handleValidationError)
app.use(handleMongooseError)
app.use(handleDatabaseError)
app.use(handleDefaultError)

app.use(express.static('../client/dist'))

app.get('*', (_req, res) => {
    res.sendFile(path.resolve('../client/dist/index.html'))
})

export default app
