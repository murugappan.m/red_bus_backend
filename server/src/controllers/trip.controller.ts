import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import { Types } from 'mongoose'
import {
    busError,
    errorWrapper,
    idError,
    tripError,
    userError,
} from '../errorResponse'

import {
    createTrip,
    deleteTripById,
    getSeats,
    getTripOfIds,
    getTripsOfOperator,
    getTripsOfQuery,
    updateTrip,
} from '../services/trip.service'
import {
    BAD_REQUEST,
    CREATION_SUCCESSFULL,
    NOT_FOUND,
    NO_CONTENT,
    OK,
} from '../statusCodes'
export const postTrip = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const {
        from,
        to,
        startDateTime,
        endDateTime,
        sleeperPrice,
        seaterPrice,
        singlePriceMarkup,
    } = req.body
    const bus = req.bus
    if (!bus) throw new Error(busError.notDefined)
    const trip = await createTrip(
        bus,
        from,
        to,
        startDateTime,
        endDateTime,
        sleeperPrice,
        seaterPrice,
        singlePriceMarkup
    )
    res.status(CREATION_SUCCESSFULL).send(trip._id)
}

export const getTripsByOperatorOrBus = async (req: Request, res: Response) => {
    const operatorId = req.user?._id
    if (!operatorId) throw new Error(userError.notDefined)
    const { busId } = req.query
    if (busId && !Types.ObjectId.isValid(busId as string))
        return res.status(BAD_REQUEST).send(errorWrapper(idError))
    const trips = await getTripsOfOperator(operatorId, busId as string | null)
    if (trips.length === 0) {
        return res.status(NOT_FOUND).send(errorWrapper(tripError.empty))
    }
    res.status(OK).send(trips)
}

export const getTripById = async (req: Request, res: Response) => {
    const { _id } = req.params
    const operatorId = req.user?._id
    if (!operatorId) throw new Error(userError.notDefined)
    const trip = await getTripOfIds(_id, operatorId)
    if (!trip) {
        return res.status(NOT_FOUND).send(errorWrapper(tripError.notFound))
    }
    res.status(OK).send(trip)
}

export const putTrip = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const {
        from,
        to,
        startDateTime,
        endDateTime,
        sleeperPrice,
        seaterPrice,
        singlePriceMarkup,
    } = req.body
    const { _id } = req.params
    const bus = req.bus
    if (!bus) throw new Error(busError.notDefined)
    const updatedTrip = await updateTrip(
        _id,
        bus,
        from,
        to,
        startDateTime,
        endDateTime,
        sleeperPrice,
        seaterPrice,
        singlePriceMarkup
    )
    if (!updatedTrip)
        res.status(BAD_REQUEST).send(errorWrapper(tripError.ticketBooked))
    res.status(NO_CONTENT).send()
}

export const deleteTrip = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const { _id } = req.params
    const deletedTrip = await deleteTripById(new Types.ObjectId(_id))
    if (!deletedTrip)
        res.status(BAD_REQUEST).send(errorWrapper(tripError.ticketBooked))
    res.status(NO_CONTENT).send()
}

export const searchTrips = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const { from, to, date, ac, type } = req.query
    const trips = await getTripsOfQuery(
        date as string,
        from as string,
        to as string,
        ac as string | undefined,
        type as string | undefined
    )
    if (trips.length === 0)
        return res
            .status(BAD_REQUEST)
            .send(errorWrapper(tripError.notFoundSearch))
    res.status(OK).send(trips)
}

export const getSeatsOfTripById = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const { _id } = req.params
    const seats = await getSeats(new Types.ObjectId(_id))
    if (seats.length === 0)
        return res.status(NOT_FOUND).send(errorWrapper(tripError.notFound))
    return res.status(OK).send(seats)
}
