export const errorWrapper = (msg: string) => ({ errors: [{ msg }] })

export const idError = 'Invalid Id'

export const userError = {
    exists: 'User already exists',
    invalid: 'Invalid Credentials',
    notDefined: 'User not defined',
}

export const tokenError = {
    notFound: 'No token, authorization denied',
    invalid: 'Token is not valid',
    notAuthorised: "you don't have previleges to perform operation",
}

export const busError = {
    notFound: 'Bus not registered',
    empty: 'No buses registered by you',
    type: 'type must be one of Sleeper, Seater or SeaterSleeper',
    registerNoRepeated: 'Bus number already registered with our platform!',
    tripInFuture:
        'trips are registered under given bus in future. Please delete trip first',
    notDefined: 'Bus not defined',
}

export const tripError = {
    notFoundSearch: 'No trips found',
    notFound: 'No such Trip',
    empty: 'no trips created by you',
    inPast: 'Journey started/completed',
    intervalClash: 'given bus already has a trip on given journey period',
    ticketBooked: 'Trip has tickets booked and cannot be modified',
    notDefined: 'Trip not defined',
}

export const ticketError = {
    notFound: 'Ticket does not exist',
    empty: 'no tickets booked by you',
    seatNotFound: (seatNo: string) => `seatNo ${seatNo} does not exist`,
    booked: 'seats already booked',
    notDefined: 'Ticket not defined',
}
