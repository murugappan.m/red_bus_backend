import { Types } from 'mongoose'

export interface User {
    name: string
    email: string
    password: string
    joiningDate: Date
    type: string
}

export interface ReqUser {
    _id: string
    type: string
}

export interface Bus {
    operatorId: Types.ObjectId
    busNo: string
    type: string
    ac: boolean
    creationDate: Date
}

export interface BusSeat {
    seatNo: string
    type: {
        single: boolean
        sleeper: boolean
        upper: boolean
    }
}

export interface Trip {
    bus: Bus
    from: string
    to: string
    startDateTime: Date
    endDateTime: Date
    seats: Seat[]
    availableSeats: string[]
    tickets: Ticket[]
    creationDate: Date
}

export interface ProcessedTrips {
    bus: Bus
    from: string
    to: string
    startDateTime: Date
    endDateTime: Date
    totalSeats: number
    availableSeats: number
}

export interface Seat {
    seatNo: string
    type: {
        sleeper: boolean
        upper: boolean
        single: boolean
    }
    price: number
}

export interface Passenger {
    name: string
    gender: string
    age: number
}

export interface ReqSeat {
    seatNo: string
    passenger: Passenger
}

export interface TicketSeat extends Seat {
    passenger: Passenger
}

export interface ResponseSeat extends Seat {
    isBooked: boolean
}

export interface Ticket {
    _id: Types.ObjectId
    userId: Types.ObjectId
    seats: TicketSeat[]
}

export interface ResTicket {
    from: string
    to: string
    startDateTime: Date
    endDateTime: Date
    seats: Seat
}
