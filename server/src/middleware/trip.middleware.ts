import { Request } from 'express-validator/src/base'
import express, { Response, NextFunction } from 'express'
import { isDateInFuture } from '../utils/validation/date'
import {
    getTripInInterval,
    getTripOfIds,
    isTripInPast,
} from '../services/trip.service'
import { getBusOfIds } from '../services/bus.service'
import { BAD_REQUEST, NOT_FOUND } from '../statusCodes'
import { busError, errorWrapper, tripError, userError } from '../errorResponse'
import { ReqSeat } from '../interface'

export const checkSeats = (seats: ReqSeat[]) => {
    if (!Array.isArray(seats) || seats.length === 0)
        throw new Error('seats must be array with atleast one seat details')
    seats.forEach((seat: ReqSeat) => {
        if (!seat.seatNo || !seat.passenger)
            throw new Error('seatNo and passenger must be specified')
        const { seatNo, passenger } = seat
        const { name, age, gender } = passenger
        if (typeof seatNo !== 'string') throw new Error('seatNo must be string')
        if (typeof name !== 'string' || name.length === 0)
            throw new Error('passenger name must be string and not empty')
        if (typeof age !== 'number' || age < 3)
            throw new Error('passenger age must be number and greater than 3')
        if (
            typeof gender !== 'string' ||
            !['Male', 'Female', 'Other'].includes(gender)
        )
            throw new Error(
                'passenger gender must be one of Male, Female or Other'
            )
    })
    const seatNos = seats.map((seat) => seat.seatNo)
    if (new Set(seatNos).size !== seatNos.length)
        throw new Error('seat number repeated')
    return true
}

export const checkEndAfterStart = (
    endDateTime: string,
    { req }: { req: Request }
) => {
    isDateInFuture(endDateTime)
    const { startDateTime } = req.body
    if (new Date(endDateTime) <= new Date(startDateTime))
        throw new Error('endDate must be greater than start date')
    return true
}

export const canModifyTrip = async (
    req: express.Request,
    res: Response,
    next: NextFunction
) => {
    const { _id } = req.params
    const operatorId = req.user?._id
    if (!operatorId) throw new Error(userError.notDefined)
    const trip = await getTripOfIds(_id, operatorId)
    if (!trip) {
        return res.status(NOT_FOUND).send(errorWrapper(tripError.notFound))
    }
    if (isTripInPast(trip)) {
        return res.status(BAD_REQUEST).send(errorWrapper(tripError.inPast))
    }
    req.trip = trip
    next()
}

export const isValidBus = async (
    req: express.Request,
    res: Response,
    next: NextFunction
) => {
    const { busId, startDateTime, endDateTime } = req.body
    const operatorId = req.user?._id
    if (!operatorId) throw new Error(userError.notDefined)
    const bus = await getBusOfIds(busId, operatorId)
    const { _id } = req.params
    if (!bus) {
        return res.status(NOT_FOUND).send(errorWrapper(busError.notFound))
    }
    if (
        await getTripInInterval(
            bus._id.toString(),
            operatorId,
            startDateTime,
            endDateTime,
            _id
        )
    ) {
        return res
            .status(BAD_REQUEST)
            .send(errorWrapper(tripError.intervalClash))
    }
    req.bus = bus
    next()
}
