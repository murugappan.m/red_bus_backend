import { Schema, model } from 'mongoose'
import { Trip } from '../interface'
import { BusModel, busSchema } from './bus.model'
import { seatSchema } from './seat.model'
import { ticketSchema } from './ticket.model'

const tripSchema = new Schema<Trip>({
    bus: {
        type: busSchema,
        required: true,
        ref: BusModel,
    },
    from: {
        type: String,
        required: true,
    },
    to: {
        type: String,
        required: true,
    },
    startDateTime: {
        type: Date,
        required: true,
    },
    endDateTime: {
        type: Date,
        required: true,
    },
    seats: {
        type: [seatSchema],
        required: true,
    },
    availableSeats: {
        type: [String],
        required: true,
    },
    tickets: {
        type: [ticketSchema],
        required: true,
    },
    creationDate: {
        type: Date,
        default: Date.now(),
    },
})

export const TripModel = model<Trip>('Trip', tripSchema)
