import app from './app'
import { connectDB } from './config/db'
import { port } from './config'
import swaggerUi from 'swagger-ui-express'
import { readFile } from 'fs/promises'

readFile('./dist/swagger_output.json', 'utf8')
    .then((swaggerFile) => {
        app.use(
            '/api/documentation',
            swaggerUi.serve,
            swaggerUi.setup(JSON.parse(swaggerFile))
        )
    })
    .catch((err) => {
        console.error(err)
        process.exit(1)
    })

connectDB()
    .then(() => {
        app.listen(port, () => {
            console.log(`[server]: Server is running at PORT:${port}`)
        })
    })
    .catch((err) => {
        console.error(err)
        process.exit(1)
    })
