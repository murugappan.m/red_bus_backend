import { FilterQuery, Types } from 'mongoose'
import { Bus, ResponseSeat, Trip } from '../interface'
import { TripModel } from '../models/trip.model'
import seatGenerator from '../utils/seatGenerator'
import toBoolean from '../utils/typeConvert/toBoolean'
import { dayFrame } from '../utils/validation/date'

export const getTripInFutureOfBus = async (
    busId: string,
    operatorId: string
) => {
    return await TripModel.findOne({
        'bus._id': busId,
        'bus.operatorId': operatorId,
        endDateTime: { $gte: new Date().toISOString() },
    })
}

export const getTripInInterval = async (
    busId: string,
    operatorId: string,
    startDateTime: string,
    endDateTime: string,
    _id: string | null = null
) => {
    return await TripModel.findOne({
        'bus._id': busId,
        _id: { $ne: _id },
        'bus.operatorId': operatorId,
        startDateTime: { $lte: endDateTime },
        endDateTime: { $gte: startDateTime },
    })
}

export const createTrip = async (
    bus: Bus,
    from: string,
    to: string,
    startDateTime: string,
    endDateTime: string,
    sleeperPrice: number,
    seaterPrice: number,
    singlePriceMarkup: number
) => {
    const seats = await seatGenerator(
        bus.type,
        sleeperPrice,
        seaterPrice,
        singlePriceMarkup
    )
    const availableSeats = seats?.map((seat) => seat.seatNo)
    const trip = new TripModel({
        bus,
        from,
        to,
        startDateTime,
        endDateTime,
        seats,
        availableSeats,
    })
    await trip.save()
    return trip
}

export const updateTrip = async (
    _id: string,
    bus: Bus,
    from: string,
    to: string,
    startDateTime: string,
    endDateTime: string,
    sleeperPrice: number,
    seaterPrice: number,
    singlePriceMarkup: number
) => {
    const seats = await seatGenerator(
        bus.type,
        sleeperPrice,
        seaterPrice,
        singlePriceMarkup
    )
    const availableSeats = seats?.map((seat) => seat.seatNo)
    return await TripModel.findOneAndUpdate(
        {
            _id,
            tickets: { $size: 0 },
        },
        {
            bus,
            from,
            to,
            startDateTime,
            endDateTime,
            seats,
            availableSeats,
        }
    )
}

export const getTripsOfOperator = async (
    operatorId: string,
    busId: string | null = null
) => {
    if (busId)
        return await TripModel.find({
            'bus.operatorId': operatorId,
            'bus._id': busId,
        })
    return await TripModel.find({ 'bus.operatorId': operatorId })
}

export const getTripOfIds = async (_id: string, operatorId: string) => {
    return await TripModel.findOne({ _id, 'bus.operatorId': operatorId })
}

export const getTripOfId = async (_id: Types.ObjectId) => {
    return await TripModel.findById(_id)
}

export const isTripInPast = (trip: Trip) => {
    const now = new Date()
    return trip.startDateTime <= now || trip.endDateTime <= now
}

export const deleteTripById = async (_id: Types.ObjectId) => {
    return await TripModel.findOneAndDelete({ _id, tickets: { $size: 0 } })
}

export const getTripsOfQuery = async (
    date: string,
    from: string,
    to: string,
    ac: string | undefined,
    type: string | undefined
) => {
    const { startDateTime, endDateTime } = dayFrame(date)
    const typeQuery: FilterQuery<Trip> = {
        from,
        to,
        startDateTime: {
            $gte: startDateTime,
            $lte: endDateTime,
        },
        availableSeats: { $exists: true, $ne: [] },
    }
    if (!(ac == null)) typeQuery['bus.ac'] = toBoolean(ac)
    if (!(type == null)) typeQuery['bus.type'] = type
    return await TripModel.find(typeQuery, {
        from: 1,
        to: 1,
        bus: 1,
        startDateTime: 1,
        endDateTime: 1,
        totalSeats: { $size: '$seats' },
        availableSeat: { $size: '$availableSeats' },
        minPrice: { $min: '$seats.price' },
        maxPrice: { $max: '$seats.price' },
    })
}

export const getSeats = async (_id: Types.ObjectId) => {
    const trip = await getTripOfId(_id)
    if (!trip) return []
    const seatArray: ResponseSeat[] = []
    for (const seat of trip.seats) {
        const responseSeat = {
            seatNo: seat.seatNo,
            type: seat.type,
            price: seat.price,
            isBooked: false,
        }
        if (!trip.availableSeats?.includes(seat.seatNo))
            responseSeat.isBooked = true
        seatArray.push(responseSeat)
    }
    return seatArray
}
