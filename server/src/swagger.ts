// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore: swagger autogen does not support typescript types
import swaggerAutogen from 'swagger-autogen'
const doc = {
    info: {
        title: 'RedBus Api',
        description: "Extensive backend api's for red bus application",
    },
    host: 'red-bus-clone.herokuapp.com',
    schemes: ['https'],
    securityDefinitions: {
        apiKeyAuth: {
            type: 'apiKey',
            in: 'header',
            name: 'x-auth-token',
            description: 'authorisation token',
        },
    },
    definitions: {
        register: {
            $name: 'operator',
            $email: 'operator1@gmail.com',
            $password: 'Operator@123',
            $type: 'Operator',
        },
        login: {
            $email: 'operator1@gmail.com',
            $password: 'Operator@123',
        },
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNjMwMzExYTE0NzUzMjAxNTdkMDIwOWNjIiwidHlwZSI6Ik9wZXJhdG9yIn0sImlhdCI6MTY2MTE0NTUwNiwiZXhwIjoxNjYxNTc3NTA2fQ.Qf2rTArAkDMmYU2xIO7YVtqRtgWCFs6JjEWhE1TfTZo',
        bus: {
            $busNo: 'TN58V5464',
            $ac: true,
            $type: 'Sleeper',
        },
        id: '62f52c5386dfecf5bbbda6c1',
        trip: {
            $busId: '62f52c5386dfecf5bbbda6c1',
            $from: 'madurai',
            $to: 'coimbatore',
            $startDateTime: '2022-10-21T10:08:54Z',
            $endDateTime: '2022-10-21T16:08:54Z',
            $sleeperPrice: 500,
            $seaterPrice: 400,
            $singlePriceMarkup: 50,
        },
        ticket: {
            $seats: [
                {
                    $seatNo: 'L8',
                    $passenger: {
                        $name: 'Murugappan M',
                        $age: 20,
                        $gender: 'Male',
                    },
                },
                {
                    $seatNo: 'L9',
                    $passenger: {
                        $name: 'Alagu',
                        $age: 25,
                        $gender: 'Female',
                    },
                },
            ],
        },
        validationError: {
            $errors: [
                {
                    $msg: 'Please include a valid email',
                },
            ],
        },
        customError: {
            $type: 'MongoError',
            $message: 'Connection Failed',
        },
        busResponse: {
            $busNo: 'TN58J7506',
            $operatorId: '3141315534',
            ac: true,
            $type: 'Sleeper',
        },
        tripResponse: {
            $from: 'madurai',
            $to: 'coimbatore',
            $startDateTime: '2022-10-21T11:08:55Z',
            $endDateTime: '"2022-10-21T16:08:55Z',
            $availableSeats: ['L1', 'U1'],
            $seats: [
                {
                    $seatNo: 'U9',
                    $price: '650',
                    $type: {
                        $sleeper: true,
                        $upper: false,
                        $single: true,
                    },
                },
            ],
            tickets: [
                {
                    $userId: '62f52c5386dfecf5bbbda6c1',
                    $seats: [
                        {
                            $seatNo: 'U9',
                            $price: '650',
                            $type: {
                                $sleeper: true,
                                $upper: false,
                                $single: true,
                            },
                            $passenger: {
                                $name: 'murugu',
                                $gender: 'Male',
                                $age: 20,
                            },
                        },
                    ],
                },
            ],
        },
        ticketResponse: {
            $from: 'madurai',
            $to: 'coimbatore',
            $busNo: 'TN58J7506',
            $startDateTime: '2022-10-21T11:08:55Z',
            $endDateTime: '"2022-10-21T16:08:55Z',
            $tripId: '62f52c5386dfecf5bbbda6c1',
            $seats: [
                {
                    $seatNo: 'U9',
                    $price: '650',
                    $type: {
                        $sleeper: true,
                        $upper: false,
                        $single: true,
                    },
                    $passenger: {
                        $name: 'murugu',
                        $gender: 'Male',
                        $age: 20,
                    },
                },
            ],
        },
        seatResponse: [
            {
                $seatNo: 'L8',
                $price: 650,
                $type: {
                    $sleeper: true,
                    $upper: false,
                    $single: true,
                },
                $booked: false,
            },
            {
                $seatNo: 'L9',
                $price: 650,
                $type: {
                    $sleeper: true,
                    $upper: false,
                    $single: true,
                },
                $booked: false,
            },
        ],
    },
}
const options = {
    autoHeaders: false,
    autoQuery: false,
    autoBody: false,
}
const outputFile = 'dist/swagger_output.json'
swaggerAutogen(options)(outputFile, ['./src/app.ts'], doc)
